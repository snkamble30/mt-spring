package com.myspring.app.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.myspring.app.service.MetricService;
import com.myspring.app.util.BaseReturn;

import lombok.extern.log4j.Log4j2;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/myspring_1")
@Log4j2

public class BaseController {

	@Autowired
	BaseReturn baseReturn;
	
	@Autowired
	MetricService metricService;
	

	@GetMapping(value = "/welcome")
	public ModelAndView welcome() {
		log.info("======== Base Controller Welcome Page ==========");		
		return new ModelAndView("welcome");
	}

	
	
	@GetMapping(value = "/doConvert/{metricType}")
	@ResponseBody
	public BaseReturn getAppName(@PathVariable String metricType, @RequestParam String convertFrom,
			@RequestParam Double convertOf) {
		log.info("============ Inside get App Name from M_SERVICE_1 ==================");
		baseReturn = metricService.doConvert(metricType, convertFrom, convertOf);
		return baseReturn;
	}
	
	
	
}