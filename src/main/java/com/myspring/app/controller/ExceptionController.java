package com.myspring.app.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.myspring.app.util.ErrorMessage;

import lombok.extern.log4j.Log4j2;

@ControllerAdvice
@Log4j2
public class ExceptionController{

	@ExceptionHandler(value=Exception.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public @ResponseBody ErrorMessage handleException(HttpServletRequest request,Exception ex) {
		log.info("Request "+request.getRequestURI() +" Threw an Exception", ex);
		ErrorMessage error= new ErrorMessage(0, new Date(), ex.toString(), request.getRequestURI());
		return error;
	}
	
}
