package com.myspring.app.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class MetricHelper {
	
	public Map<String, Double> getTemperature(String metricFrom, Double number) {
		Map<String, Double> conversionMap = new HashMap<>();
		if (metricFrom.equals("celcious")) {
			conversionMap.put("celToFer", (number * (9 / 5)) + 32);
			conversionMap.put("celToKel", (number + 273.15));
		} else if (metricFrom.equals("feran")) {
			conversionMap.put("ferToCel", (number - 32) * 5 / 9 + 273.15);
			conversionMap.put("ferToKel", (number - 32) * 5 / 9 + 273.15);
		} else {
			conversionMap.put("kelToCel", number - 273.15);
			conversionMap.put("kelToFer", number - 273.15);
		}
		return conversionMap;
	}
	
	public Map<String, Double> getLengths(String metricFrom, Double number) {
		Map<String, Double> conversionMap = new HashMap<>();
		if (metricFrom.equals("centi")) {
			conversionMap.put("centiTomiliMeter", (number * 10 ));
			conversionMap.put("centiToMeter", (number /100 ));
			conversionMap.put("centiToKm", (number /100000 ));
			conversionMap.put("centiToMile", (number /160934 ));
			conversionMap.put("centiToFoot", (number /30.48 ));
			conversionMap.put("centiToInch", (number /2.54 ));
			
		} else if (metricFrom.equals("meter")) {
			conversionMap.put("meterToMiliMeter", (number * 1000 ));
			conversionMap.put("meterToCentiMeter", (number * 100 ));
			conversionMap.put("meterToKm", (number / 1000 ));
			conversionMap.put("meterToMile", (number /1609 ));
			conversionMap.put("meterToFoot", (number /3.281 ));
			conversionMap.put("meterToInch", (number /39.37 ));
			
		} else if (metricFrom.equals("km")) {
			conversionMap.put("kmToMiliMeter", (number * 1000 ));
			conversionMap.put("kmToCentiMeter", (number * 100000 ));
			conversionMap.put("kmToMile", (number / 1.609 ));
			conversionMap.put("kmToFoot", (number * 3281 ));
			conversionMap.put("kmToInch", (number * 39370 ));
		}
		return conversionMap;
	}

}
