package com.myspring.app.service;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.myspring.app.util.BaseReturn;


@Service
public interface MetricService{
	
	public BaseReturn doConvert(String metricType,String convertFrom, Double ofNumber);
	
	
}
