package com.myspring.app.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myspring.app.util.BaseReturn;
import com.myspring.app.util.RestErrors;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class MetricServiceImpl implements MetricService {
	
	@Autowired
	MetricHelper metricHelper;
	
	@Override
	public BaseReturn doConvert(String metricType, String convertFrom, Double ofNumber) {
		metricHelper.getTemperature(convertFrom, ofNumber);
		Map<String, Double> dataMap = new HashMap<>();
		if(metricType.equals("temp")) {
			dataMap = metricHelper.getTemperature(convertFrom, ofNumber);
			
		}else if(metricType.equals("length")) {
			dataMap = metricHelper.getLengths(convertFrom, ofNumber);
			
		}
		
		return new BaseReturn(RestErrors.SUCCESS.getErrorCode(),RestErrors.SUCCESS.getHttpStatus(),dataMap);
		
	}
	

}
