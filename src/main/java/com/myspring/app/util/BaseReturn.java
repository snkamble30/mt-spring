package com.myspring.app.util;

import org.springframework.stereotype.Component;


@Component
public class BaseReturn {
		private String responseCode; // 00,01,02 Etc
		private int respnseStatus; // Success,Fail,Pending Etc
		private String responseMsg; // Request Processed successfully. Etc
		private String responseData; // Data to send in Json Format
		private Object dataObj=new Object(); // Data List to be send
		
		
		
		
		public BaseReturn() {
			super();
		}


		public BaseReturn(String responseCode, int respnseStatus, Object dataObj) {
			super();
			this.responseCode = responseCode;
			this.respnseStatus = respnseStatus;
			this.dataObj = dataObj;
		}


		public String getResponseCode() {
			return responseCode;
		}


		public void setResponseCode(String responseCode) {
			this.responseCode = responseCode;
		}


		public int getRespnseStatus() {
			return respnseStatus;
		}


		public void setRespnseStatus(int respnseStatus) {
			this.respnseStatus = respnseStatus;
		}


		public String getResponseMsg() {
			return responseMsg;
		}


		public void setResponseMsg(String responseMsg) {
			this.responseMsg = responseMsg;
		}


		public String getResponseData() {
			return responseData;
		}


		public void setResponseData(String responseData) {
			this.responseData = responseData;
		}


		public Object getDataObj() {
			return dataObj;
		}


		public void setDataObj(Object dataObj) {
			this.dataObj = dataObj;
		}

		
		
}
