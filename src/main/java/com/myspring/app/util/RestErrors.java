package com.myspring.app.util;

public enum RestErrors {
	SUCCESS("00", 200, "Request Processed Successfully"),
	MISSING_REQUEST_PARAM("01", 422, ""),
	UNAUTHORIZED_ACCESS("02", 401, ""),
	INVALID_REQUEST_PARAM("03", 422, ""),
	NO_DATA_FOUND("04", 404, ""),
	ALREADY_EXISTS("05", 422, ""),
	ERROR_OCCURED("06", 500, ""),
	BAD_REQUEST("07", 400, ""),
	BLOCKED_USER("08", 403, ""),
	DUPLICATE_RECORD("09", 403, ""),
	RESOURCE_NOT_FOUND("404", 404, ""),
	CLIENT_RESPONSE_FAILED("10", 422, ""),
	CLIENT_UNABLE_TO_CONNECT("11", 422, ""),
	NO_DATA_FOUND1("04", 422, ""),
	INVALID_OTP("111", 422, ""),
	OTP_LIMIT("112", 422, "");

	private String errorCode;
	private int httpStatus;
	private String errorMessage;

	RestErrors(String errorCode, int httpStatus, String errorMessage) {
		this.errorCode = errorCode;
		this.httpStatus = httpStatus;
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public int getHttpStatus() {
		return httpStatus;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
